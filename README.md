# recv-dir

Simple recursive directory traversal.

## Description

Traverse directories recursively and conditionally using iterators.

## Example

### Visit all directories

```rust
fn main() {
    use recv_dir::RecursiveDirIterator;
    let dir = RecursiveDirIterator::from_root("test_dir").unwrap();

    for entry in dir {
        println!("{:?}", entry);
    }
}
```

### Ignore symlinks

```rust
fn main() {
    use std::path::Path;
    use recv_dir::RecursiveDirIterator;
    let dir = RecursiveDirIterator::with_closure_filter("test_dir", |dir: &Path| !dir.is_symlink()).unwrap();

    for entry in dir {
        println!("{:?}", entry);
    }
}
```

### Control depth

Visits only the files in `test_dir`:

```rust
fn main() {
    use std::path::{Path, PathBuf};
    use recv_dir::RecursiveDirIterator;
    let root = PathBuf::from("test_dir");
    let ancestors = root.ancestors().count();
    let dir = RecursiveDirIterator::with_closure_filter(root, |dir: &Path| dir.ancestors().count() - ancestors <= 1).unwrap();

    for entry in dir {
        println!("{:?}", entry);
    }
}
```

Visits the files in `test_dir` and in first-level subdirectories:

```rust
fn main() {
    use std::path::{Path, PathBuf};
    use recv_dir::RecursiveDirIterator;
    let root = PathBuf::from("test_dir");
    let ancestors = root.ancestors().count();
    let dir = RecursiveDirIterator::with_closure_filter(root, |dir: &Path| dir.ancestors().count() - ancestors <= 2).unwrap();

    for entry in dir {
        println!("{:?}", entry);
    }
}
```

You can also compose the filters:

```rust
fn main() {
    use std::num::NonZeroUsize;
    use std::path::{Path, PathBuf};
    use recv_dir::{Filter, MaxDepth, NoSymlink, RecursiveDirIterator};
    let root = PathBuf::from("test_dir");
    let dir = RecursiveDirIterator::with_filter(
        &root,
        NoSymlink.and(MaxDepth::new(&root, NonZeroUsize::new(2).unwrap())),
    ).unwrap();

    for entry in dir {
        println!("{:?}", entry);
    }
}
```

**Read the [documentation](https://docs.rs/recv-dir) for more information**