//! # recv-dir
//!
//! Simple recursive directory traversal.
//!
//! ## Usage
//!
//! **recv-dir** implements a simple directory traversal, which supports a selective directory visit.
//!
//! Since it is a simple iterator, you can use regular iterator functions like `filter` and `map` to
//! ignore individual files or directories, but it does not prevent them from being visited.
//!
//! To ignore directories from being visited, you will need to use a filtered iterator,
//! like the examples below.
//!
//! ## Example
//!
//! ### Visit all directories
//!
//! ```rust
//! use recv_dir::RecursiveDirIterator;
//! let dir = RecursiveDirIterator::from_root("test_dir").unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! ### Ignore symlinks
//!
//! ```rust
//! use std::path::Path;
//! use recv_dir::{NoSymlink, RecursiveDirIterator};
//! let dir = RecursiveDirIterator::with_filter("test_dir", NoSymlink).unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! ### Control depth
//!
//! Visit only the files in `test_dir`:
//!
//! ```rust
//! use std::path::{Path, PathBuf};
//! use recv_dir::{MaxDepth, RecursiveDirIterator};
//! let root = PathBuf::from("test_dir");
//! let dir = RecursiveDirIterator::with_filter(root, MaxDepth::single_depth()).unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! Visits the files in `test_dir` and in first-level subdirectories:
//!
//! ```rust
//! use std::num::NonZeroUsize;
//! use std::path::{Path, PathBuf};
//! use recv_dir::{MaxDepth, RecursiveDirIterator};
//! let root = PathBuf::from("test_dir");
//! let dir = RecursiveDirIterator::with_filter(root, MaxDepth::new(NonZeroUsize::new(2).unwrap())).unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! ### Control file emission
//!
//! Iterator only yields files with the `.rs` extension:
//!
//! ```rust
//! use std::path::{Path, PathBuf};
//! use recv_dir::{Extension, RecursiveDirIterator};
//! let root = PathBuf::from("test_dir");
//! let dir = RecursiveDirIterator::with_filter(root, Extension::new("rs")).unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! ### Filter composing
//!
//! You can also compose the filters:
//!
//! ```rust
//! use std::num::NonZeroUsize;
//! use std::path::{Path, PathBuf};
//! use recv_dir::{Filter, MaxDepth, NoSymlink, RecursiveDirIterator};
//! let root = PathBuf::from("test_dir");
//! let dir = RecursiveDirIterator::with_filter(&root, NoSymlink.and(MaxDepth::new(NonZeroUsize::new(2).unwrap()))).unwrap();
//!
//! for entry in dir {
//!    println!("{:?}", entry);
//! }
//! ```
//!
//! ## Order
//!
//! There is no guarantee on the order of the traversal,
//! it can be OS-dependent, File System-dependent or completely arbitrary.
//!
//! **recv-dir** currently exhibits a breadth-first traversal order because of its current
//! implementation, but this is not a guarantee and might change.
//!
//! ## Failures
//!
//! Walking the directory tree may fail, and at the moment, **recv-dir** will only fail if the base directory
//! cannot be visited and will silently ignore the subdirectories that are not accessible
//! or fails to open.
//!
//! ## Recursion
//!
//! **recv-dir** uses a stack to keep track of directories to visit.
//!
//! It works by pushing the directories to be visited in a stack, and once it finishes all the files
//! in the current level, it pops a directory and continues from there.
//!
//! Since it is stack-based, it will never crash because of recursion limits, and may also
//! not crash because of memory exhaustion. Because of this, if you do have symlink visiting enabled,
//! the iteration can fall into an infinite symlink recursion, which will cause high-cpu usage. In
//! certain scenarios, the stack may grow and exhaust all the available heap memory, which may result
//! in an OOM kill, or even worse, a complete system hang.
//!
//! To avoid this, you can either configure a maximum depth or never visit symlinks, since, at the moment,
//! **recv-dir** does not provide any protection against infinite symlink recursion.
//!
//! ## Experiment
//!
//! **This project is part of the [Kores/rust-experiments](https://gitlab.com/Kores/rust-experiments).**
//!

mod filter;

pub use crate::filter::{Accept, Closure, Extension, Filter, MaxDepth, NoSymlink, This};
#[cfg(not(feature = "tokio"))]
use std::fs::{read_dir, ReadDir};
use std::num::NonZeroUsize;
use std::path::{Path, PathBuf};
#[cfg(feature = "tokio")]
use tokio::fs::{read_dir, ReadDir};

/// Reads the directory recursively without any filter.
///
/// Beware the risks of traversing a directory without any filter, like
/// infinite symlink recursion.
#[cfg(not(feature = "tokio"))]
pub fn recursive_read_dir<P: AsRef<Path>>(
    dir: P,
) -> std::io::Result<impl Iterator<Item = PathBuf>> {
    RecursiveDirIterator::from_root(dir)
}

/// Reads the directory recursively without any filter.
///
/// Beware the risks of traversing a directory without any filter, like
/// infinite symlink recursion.
#[cfg(feature = "tokio")]
pub async fn recursive_read_dir<P: AsRef<Path>>(dir: P) -> std::io::Result<RecursiveDirIterator> {
    RecursiveDirIterator::from_root(dir).await
}

/// Holds the state of the directory recursion.
pub struct RecursiveDirIterator<F = Accept> {
    base_dir: PathBuf,
    current_iter: Option<ReadDir>,
    dirs: Vec<PathBuf>,
    visit_filter: F,
}

impl RecursiveDirIterator {
    /// Creates an iterator that starts right from the provided directory.
    ///
    /// The iterator will visit all directories, regardless if they are symlinks or not.
    #[cfg(not(feature = "tokio"))]
    pub fn from_root(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: Accept,
        })
    }

    /// Creates an iterator that starts right from the provided directory.
    ///
    /// The iterator will visit all directories, regardless if they are symlinks or not.
    #[cfg(feature = "tokio")]
    pub async fn from_root(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: Accept,
        })
    }
}

impl RecursiveDirIterator<NoSymlink> {
    /// Creates an iterator that starts right from the provided directory.
    ///
    /// The iterator will visit all directories but will ignore any symlinks.
    #[cfg(not(feature = "tokio"))]
    pub fn from_root_no_symlinks(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: NoSymlink,
        })
    }

    /// Creates an iterator that starts right from the provided directory.
    ///
    /// The iterator will visit all directories but will ignore any symlinks.
    #[cfg(feature = "tokio")]
    pub async fn from_root_no_symlinks(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: NoSymlink,
        })
    }
}

impl RecursiveDirIterator<MaxDepth> {
    /// Creates an iterator that starts right from the provided directory up to a max depth.
    #[cfg(not(feature = "tokio"))]
    pub fn from_root_max_depth(
        path: impl AsRef<Path>,
        max_depth: NonZeroUsize,
    ) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: MaxDepth(max_depth),
        })
    }

    /// Creates an iterator that starts right from the provided directory up to a max depth.
    #[cfg(not(feature = "tokio"))]
    pub fn from_root_single_depth(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: MaxDepth::single_depth(),
        })
    }

    /// Creates an iterator that starts right from the provided directory up to a max depth.
    #[cfg(feature = "tokio")]
    pub async fn from_root_max_depth(
        path: impl AsRef<Path>,
        max_depth: NonZeroUsize,
    ) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: MaxDepth(max_depth),
        })
    }

    /// Creates an iterator that starts right from the provided directory up to a max depth.
    #[cfg(feature = "tokio")]
    pub async fn from_root_single_depth(path: impl AsRef<Path>) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: MaxDepth::single_depth(),
        })
    }
}

impl<F> RecursiveDirIterator<Closure<F>> {
    /// Creates an iterator that starts right from the provided directory with a specific filter
    /// to decide which directories to visit.
    #[cfg(not(feature = "tokio"))]
    pub fn with_closure_filter(path: impl AsRef<Path>, filter: F) -> std::io::Result<Self>
    where
        F: Into<Closure<F>>,
    {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: filter.into(),
        })
    }

    #[cfg(feature = "tokio")]
    pub async fn with_closure_filter(path: impl AsRef<Path>, filter: F) -> std::io::Result<Self>
    where
        F: Into<Closure<F>>,
    {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: filter.into(),
        })
    }
}

impl<F> RecursiveDirIterator<This<F>> {
    /// Creates an iterator that starts right from the provided directory with a specific filter
    /// to decide which directories to visit.
    #[cfg(not(feature = "tokio"))]
    pub fn with_filter(path: impl AsRef<Path>, filter: F) -> std::io::Result<Self>
    where
        F: Filter,
    {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: This::new(filter),
        })
    }

    /// Creates an iterator that starts right from the provided directory with a specific filter
    /// to decide which directories to visit.
    #[cfg(feature = "tokio")]
    pub async fn with_filter(path: impl AsRef<Path>, filter: F) -> std::io::Result<Self>
    where
        F: Filter,
    {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: This::new(filter),
        })
    }
}

impl<S> RecursiveDirIterator<Extension<S>> {
    /// Creates an iterator that only emits the paths with the provided extension.
    ///
    /// Read more at [`Extension`] documentation.
    #[cfg(not(feature = "tokio"))]
    pub fn with_extension(path: impl AsRef<Path>, extension: S) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path)?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: Extension::new(extension),
        })
    }

    /// Creates an iterator that only emits the paths with the provided extension.
    ///
    /// Read more at [`Extension`] documentation.
    #[cfg(feature = "tokio")]
    pub async fn with_extension(path: impl AsRef<Path>, extension: S) -> std::io::Result<Self> {
        let base_dir = path.as_ref().to_path_buf();
        let r = read_dir(path).await?;

        Ok(RecursiveDirIterator {
            base_dir,
            current_iter: Some(r),
            dirs: vec![],
            visit_filter: Extension::new(extension),
        })
    }
}

#[cfg(not(feature = "tokio"))]
impl<F> Iterator for RecursiveDirIterator<F>
where
    F: Filter,
{
    type Item = PathBuf;

    fn next(&mut self) -> Option<Self::Item> {
        fn compute_next_iter<VF>(iter: &mut RecursiveDirIterator<VF>) -> Option<ReadDir> {
            while let Some(ref p) = iter.dirs.pop() {
                if let Ok(d) = read_dir(p) {
                    return Some(d);
                }
            }
            None
        }

        if self.current_iter.is_none() {
            self.current_iter = compute_next_iter(self);
        }

        while let Some(ref mut iter) = self.current_iter {
            for n in iter.flatten().by_ref() {
                let path = n.path();

                if path.is_dir() && self.visit_filter.filter(&self.base_dir, &path) {
                    self.dirs.push(path.clone());
                }

                if self.visit_filter.should_emit(&path) {
                    return Some(path.clone());
                }
            }

            self.current_iter = compute_next_iter(self);
        }

        None
    }
}

#[cfg(feature = "tokio")]
impl<F> RecursiveDirIterator<F>
where
    F: Filter,
{
    pub async fn next(&mut self) -> Option<PathBuf> {
        async fn compute_next_iter<VF>(iter: &mut RecursiveDirIterator<VF>) -> Option<ReadDir> {
            while let Some(ref p) = iter.dirs.pop() {
                if let Ok(d) = read_dir(p).await {
                    return Some(d);
                }
            }
            None
        }

        if let None = self.current_iter {
            self.current_iter = compute_next_iter(self).await;
        }

        while let Some(ref mut iter) = self.current_iter {
            loop {
                let next_entry = iter.next_entry().await;
                match next_entry {
                    Ok(Some(n)) => {
                        let path = n.path();

                        if path.is_dir() {
                            if self.visit_filter.filter(&self.base_dir, &path) {
                                self.dirs.push(path.clone());
                            }
                        }

                        if self.visit_filter.should_emit(&path) {
                            return Some(path.clone());
                        }
                    }
                    Ok(None) => {
                        break;
                    }
                    Err(_) => {
                        // TODO: handle this properly.
                    }
                }
            }

            self.current_iter = compute_next_iter(self).await;
        }

        None
    }
}

#[cfg(test)]
#[cfg(not(feature = "tokio"))]
mod tests {
    use crate::filter::Extension;
    use crate::{Filter, MaxDepth, NoSymlink, RecursiveDirIterator};
    use std::collections::HashSet;
    use std::num::NonZeroUsize;
    use std::path::{Path, PathBuf};

    #[test]
    fn all_files() {
        let mut dirs = HashSet::new();
        let from_test_dir = RecursiveDirIterator::from_root("test_dir").unwrap();

        for x in from_test_dir {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 18);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/meow/oof"));
        assert!(dirs.contains("test_dir/baz/meow/uuf"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/dragonborn/witchcraft"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/fus/ro/dah"));
        assert!(dirs.contains("test_dir/fus/ro/dah/dovahkiin"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f/foa"));
    }

    #[test]
    fn no_symlinks() {
        let mut dirs = HashSet::new();
        let from_test_dir =
            RecursiveDirIterator::with_closure_filter("test_dir", |_: &Path, p: &Path| {
                !p.is_symlink()
            })
            .unwrap();

        for x in from_test_dir {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 18);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/meow/oof"));
        assert!(dirs.contains("test_dir/baz/meow/uuf"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/dragonborn/witchcraft"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/fus/ro/dah"));
        assert!(dirs.contains("test_dir/fus/ro/dah/dovahkiin"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f/foa"));
    }

    #[test]
    fn depth_1() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let ancestors = root.ancestors().count();

        let from_test_dir =
            RecursiveDirIterator::with_closure_filter(&root, |_: &Path, p: &Path| {
                p.ancestors().count() - ancestors == 0
            })
            .unwrap();

        for x in from_test_dir {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 5);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
    }

    #[test]
    fn depth_2() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let ancestors = root.ancestors().count();

        let from_test_dir =
            RecursiveDirIterator::with_closure_filter(&root, |_: &Path, p: &Path| {
                p.ancestors().count() - ancestors <= 1
            })
            .unwrap();

        for x in from_test_dir {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 12);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
    }

    #[test]
    fn no_symlink_depth_2() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let _ancestors = root.ancestors().count();

        let from_test_dir = RecursiveDirIterator::with_filter(
            &root,
            NoSymlink.and(MaxDepth::new(NonZeroUsize::new(2).unwrap())),
        )
        .unwrap();

        for x in from_test_dir {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 12);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
    }

    #[test]
    fn file_with_extension() {
        let mut dirs = Vec::new();

        let root = PathBuf::from("test_dir");

        let from_test_dir = RecursiveDirIterator::with_filter(&root, Extension::new("i")).unwrap();

        for x in from_test_dir {
            dirs.push(x.to_string_lossy().to_string());
        }

        dirs.sort();

        assert_eq!(dirs.len(), 1);
        assert_eq!(
            dirs,
            vec![String::from("test_dir/one_more_weird_dir/file.i")]
        );
    }

    #[test]
    fn path_with_extension() {
        let mut dirs = Vec::new();

        let root = PathBuf::from("test_dir");

        let from_test_dir = RecursiveDirIterator::with_filter(&root, Extension::new("f")).unwrap();

        for x in from_test_dir {
            dirs.push(x.to_string_lossy().to_string());
        }

        dirs.sort();

        assert_eq!(dirs.len(), 1);
        assert_eq!(
            dirs,
            vec![String::from(
                "test_dir/one_more_weird_dir/another_weird_dir.f"
            )]
        );
    }
}

#[cfg(test)]
#[cfg(feature = "tokio")]
mod tests {
    use crate::filter::Extension;
    use crate::{Filter, MaxDepth, NoSymlink, RecursiveDirIterator};
    use std::collections::HashSet;
    use std::num::NonZeroUsize;
    use std::path::{Path, PathBuf};

    #[tokio::test]
    async fn tokio_all_files() {
        let mut dirs = HashSet::new();
        let mut from_test_dir = RecursiveDirIterator::from_root("test_dir").await.unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 18);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/meow/oof"));
        assert!(dirs.contains("test_dir/baz/meow/uuf"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/dragonborn/witchcraft"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/fus/ro/dah"));
        assert!(dirs.contains("test_dir/fus/ro/dah/dovahkiin"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f/foa"));
    }

    #[tokio::test]
    async fn tokio_no_symlinks() {
        let mut dirs = HashSet::new();
        let mut from_test_dir =
            RecursiveDirIterator::with_closure_filter("test_dir", |_: &Path, p: &Path| {
                !p.is_symlink()
            })
            .await
            .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 18);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/meow/oof"));
        assert!(dirs.contains("test_dir/baz/meow/uuf"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/dragonborn/witchcraft"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/fus/ro/dah"));
        assert!(dirs.contains("test_dir/fus/ro/dah/dovahkiin"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/another_weird_dir.f/foa"));
    }

    #[tokio::test]
    async fn tokio_depth_1() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let ancestors = root.ancestors().count();

        let mut from_test_dir =
            RecursiveDirIterator::with_closure_filter(&root, |_: &Path, p: &Path| {
                p.ancestors().count() - ancestors <= 0
            })
            .await
            .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 5);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
    }

    #[tokio::test]
    async fn tokio_depth_2() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let ancestors = root.ancestors().count();

        let mut from_test_dir =
            RecursiveDirIterator::with_closure_filter(&root, |_: &Path, p: &Path| {
                p.ancestors().count() - ancestors <= 1
            })
            .await
            .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 12);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
    }

    #[tokio::test]
    async fn tokio_no_symlink_depth_2() {
        let mut dirs = HashSet::new();

        let root = PathBuf::from("test_dir");
        let _ancestors = root.ancestors().count();

        let mut from_test_dir = RecursiveDirIterator::with_filter(
            &root,
            NoSymlink.and(MaxDepth::new(NonZeroUsize::new(2).unwrap())),
        )
        .await
        .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.insert(x.to_string_lossy().to_string());
        }

        assert_eq!(dirs.len(), 12);
        assert!(dirs.contains("test_dir/foo"));
        assert!(dirs.contains("test_dir/bar"));
        assert!(dirs.contains("test_dir/baz"));
        assert!(dirs.contains("test_dir/fus"));
        assert!(dirs.contains("test_dir/baz/meow"));
        assert!(dirs.contains("test_dir/baz/barbaz"));
        assert!(dirs.contains("test_dir/baz/foobaz"));
        assert!(dirs.contains("test_dir/fus/dragonborn"));
        assert!(dirs.contains("test_dir/fus/ro"));
        assert!(dirs.contains("test_dir/one_more_weird_dir"));
        assert!(dirs.contains("test_dir/one_more_weird_dir/file.i"));
    }

    #[tokio::test]
    async fn tokio_file_with_extension() {
        let mut dirs = Vec::new();

        let root = PathBuf::from("test_dir");

        let mut from_test_dir = RecursiveDirIterator::with_filter(&root, Extension::new("i"))
            .await
            .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.push(x.to_string_lossy().to_string());
        }

        dirs.sort();

        assert_eq!(dirs.len(), 1);
        assert_eq!(
            dirs,
            vec![String::from("test_dir/one_more_weird_dir/file.i")]
        );
    }

    #[tokio::test]
    async fn tokio_path_with_extension() {
        let mut dirs = Vec::new();

        let root = PathBuf::from("test_dir");

        let mut from_test_dir = RecursiveDirIterator::with_filter(&root, Extension::new("f"))
            .await
            .unwrap();

        while let Some(x) = from_test_dir.next().await {
            dirs.push(x.to_string_lossy().to_string());
        }

        dirs.sort();

        assert_eq!(dirs.len(), 1);
        assert_eq!(
            dirs,
            vec![String::from(
                "test_dir/one_more_weird_dir/another_weird_dir.f"
            )]
        );
    }
}
